//
//  AppModel.swift
//  IfFound
//
//  Created by Darren Stone on 2015-03-03.
//  Copyright (c) 2015 Darren Stone. All rights reserved.
//

import Foundation

class AppModel : NSObject
{
    static let sharedInstance = AppModel()

    private var _lockScreenText : String = ""

    dynamic var lockScreenText: String {
        get { return _lockScreenText }
        set(newValue) { _lockScreenText = newValue.trim() }
    }

    var copyright : String {
        let info: NSDictionary = NSBundle.mainBundle().infoDictionary!
        //var displayName = info["CFBundleDisplayName"] as! String
        let shortVersion = info["CFBundleShortVersionString"] as! String
        let build = info["CFBundleVersion"] as! String
        return "v\(shortVersion) (\(build))\n\u{00a9} Darren Stone 2015"
    }

    override init()
    {
        super.init()
        var lockScreenText: String! = NSUserDefaults.standardUserDefaults().stringForKey(Keys.lockScreenText)
        if lockScreenText == nil
        {
            lockScreenText = ""
        }
        self.lockScreenText = lockScreenText
    }

    func save()
    {
        NSUserDefaults.standardUserDefaults().setObject(self.lockScreenText, forKey: Keys.lockScreenText)
    }
}


extension AppModel
{
    private struct Keys
    {
        static var lockScreenText = "LockScreenText"
    }
    struct KVO
    {
        static let lockScreenText = "lockScreenText"
    }

}