//
//  Utils.swift
//  IfFound
//
//  Created by Darren Stone on 2015-03-03.
//  Copyright (c) 2015 Darren Stone. All rights reserved.
//

import UIKit

func *(edges: UIEdgeInsets, mult: CGFloat) -> UIEdgeInsets
{
    return UIEdgeInsets(top: edges.top * mult, left: edges.left * mult, bottom: edges.bottom * mult, right: edges.right * mult)
}

func *(lhs: Double, rhs: UInt64) -> Double
{
    return lhs * Double(rhs)
}

//
// CGPoint
//

func +(obj1: CGPoint, obj2: CGPoint) -> CGPoint
{
    return CGPoint(x: obj1.x + obj2.x, y: obj1.y + obj2.y)
}

func -(obj1: CGPoint, obj2: CGPoint) -> CGPoint
{
    return CGPoint(x: obj1.x - obj2.x, y: obj1.y - obj2.y)
}

func -=(inout obj1: CGPoint, obj2: CGPoint)
{
    obj1.x -= obj2.x
    obj1.y -= obj2.y
}

func +=(inout obj1: CGPoint, obj2: CGPoint)
{
    obj1.x += obj2.x
    obj1.y += obj2.y
}

//
// CGRect
//
extension CGRect
{
    mutating func ds_centerXInsideBounds(bounds: CGRect)
    {
        self.origin.x = round((bounds.size.width / 2.0) - (self.size.width / 2.0))
    }

    mutating func ds_centerXInsideBounds(bounds: CGRect, offset: CGFloat)
    {
        self.origin.x = round((bounds.size.width / 2.0) - (self.size.width / 2.0)) + offset
    }
    
    mutating func ds_centerYInsideBounds(bounds: CGRect)
    {
        self.origin.y = round((bounds.size.height / 2.0) - (self.size.height / 2.0))
    }

    mutating func ds_centerYInsideBounds(bounds: CGRect, offset: CGFloat)
    {
        self.origin.y = round((bounds.size.height / 2.0) - (self.size.height / 2.0)) + offset
    }
    
    mutating func ds_maxYInsideBounds(bounds: CGRect)
    {
        self.origin.y = bounds.size.height - self.size.height
    }

    mutating func ds_maxYInsideBounds(bounds: CGRect, offset: CGFloat)
    {
        self.origin.y = bounds.size.height - self.size.height - offset
    }
    
    mutating func ds_maxXInsideBounds(bounds: CGRect)
    {
        self.origin.x = bounds.size.width - self.size.width
    }

    mutating func ds_maxXInsideBounds(bounds: CGRect, offset: CGFloat)
    {
        self.origin.x = bounds.size.width - self.size.width - offset
    }
    
}

func +(rect: CGRect, offset: CGPoint) -> CGRect
{
    return CGRect(origin: rect.origin + offset, size: rect.size)
}

func -(rect: CGRect, offset: CGPoint) -> CGRect
{
    return CGRect(origin: rect.origin - offset, size: rect.size)
}

//
// GCD
//

/*
void DispatchMainAfterDelay(NSTimeInterval delay, void (^handler)())
{
dispatch_time_t dispatchTime = dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC);
dispatch_after(dispatchTime, dispatch_get_main_queue(), handler);
}
*/

func DispatchMain(afterDelay delay: NSTimeInterval, handler: dispatch_block_t)
{
    let dispatchTime = dispatch_time(DISPATCH_TIME_NOW, Int64(delay * NSEC_PER_SEC))
    dispatch_after(dispatchTime, dispatch_get_main_queue(), handler)
}

/*
void DispatchMain(void (^Handler)())
{
dispatch_async(dispatch_get_main_queue(), Handler);
}
*/

func DispatchMain(handler: dispatch_block_t)
{
    dispatch_async(dispatch_get_main_queue(), handler)
}

/*
void DispatchBackground(void (^Handler)())
{
dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), Handler);
}
*/

func DispatchBackground(handler: dispatch_block_t)
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), handler)
}

extension UIDevice
{
    func isIPad() -> Bool
    {
        return self.userInterfaceIdiom == .Pad
    }
}

//
// String
//

extension String
{
    func trim() -> String
    {
        return self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
    }
}
