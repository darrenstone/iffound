//
//  PhotoTextView.swift
//  IfFound
//
//  Created by Darren Stone on 2015-03-03.
//  Copyright (c) 2015 Darren Stone. All rights reserved.
//

import UIKit

class PhotoTextView: UIView
{
    private let _label = UILabel()
    private let _textInsets = UIEdgeInsets(top: 5, left: 20, bottom: 5, right: 20)
    private let _backgroundInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    private let _fontSize: CGFloat = 12.0
    private let _radius: CGFloat = 6.0
    private let _backgroundColor = UIColor(white: 0.0, alpha: 0.5)

    var text : String {
        get {
            return _label.text!
        }
        set(newText) {
            self.setNeedsLayout()
            self.setNeedsDisplay()
            _label.text = newText
        }
    }

    override init(frame: CGRect)
    {
        super.init(frame: frame)
        _label.text = ""
        _label.textAlignment = .Center
        _label.textColor = UIColor.whiteColor()
        _label.numberOfLines = 0
        _label.font = UIFont.systemFontOfSize(_fontSize)
        self.addSubview(_label)

        self.backgroundColor = UIColor.clearColor()
    }

    required init(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }

    override func drawRect(rect: CGRect)
    {
        let path = UIBezierPath(roundedRect: UIEdgeInsetsInsetRect(self.bounds, _backgroundInsets), cornerRadius: _radius)
        _backgroundColor.setFill()
        path.fill()
    }

    override func layoutSubviews()
    {
        let labelFrame = UIEdgeInsetsInsetRect(self.bounds, _textInsets)
        _label.frame = labelFrame
    }

    override func sizeThatFits(size: CGSize) -> CGSize
    {
        var bounds = CGRect(origin: CGPointZero, size: size)
        bounds.size.height = 400.0
        if bounds.size.width <= 0.0
        {
            bounds.size.width = UIScreen.mainScreen().bounds.size.width
        }
        bounds = UIEdgeInsetsInsetRect(bounds, _textInsets)
        var textRect = _label.textRectForBounds(bounds, limitedToNumberOfLines: 0)
        textRect = UIEdgeInsetsInsetRect(textRect, _textInsets * -1.0)
        return textRect.size
    }

    func sizeToFit(width width: CGFloat)
    {
        var newFrame = self.frame
        newFrame.size = self.sizeThatFits(CGSizeMake(width, 0.0))
        self.frame = newFrame
    }

    override func sizeToFit()
    {
        self.sizeToFit(width: 0.0)
    }
}

