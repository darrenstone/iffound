//
//  PhotoCollectionViewController.swift
//  IfFound
//
//  Created by Darren Stone on 2015-03-01.
//  Copyright (c) 2015 Darren Stone. All rights reserved.
//

import UIKit

let reuseIdentifier = "Cell"

class PhotoCell : UICollectionViewCell
{
    @IBOutlet var imageView : UIImageView!
}

class PhotoCollectionViewController: UICollectionViewController {

    private var _images : [UIImage] = []
    private var _photoEditor : PhotoViewController?

    override func viewWillAppear(animated: Bool) {
        let viewSize = self.view.bounds.size
        let layout = self.collectionView!.collectionViewLayout as! UICollectionViewFlowLayout
        var itemSize = CGSize()
        itemSize.width = (viewSize.width / 3.0) - 1.0
        itemSize.height = itemSize.width
        layout.itemSize = itemSize
    }

    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowPhoto"
        {
            let cell = sender as! PhotoCell
            let photoViewer = segue.destinationViewController as! PhotoViewController
            photoViewer.photo = cell.imageView.image
        }
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return _images.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! PhotoCell
        cell.imageView.image = _images[indexPath.row]
        return cell
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */
}

extension PhotoCollectionViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    @IBAction func choosePhoto(sender: AnyObject?)
    {
        let chooser = UIImagePickerController()
        chooser.delegate = self // TODO: Crashes Swift 2 Beta 1 with optimization enabled.
        self.presentViewController(chooser, animated: true, completion: nil)
    }

    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?)
    {
        self.dismissViewControllerAnimated(true, completion: nil)

        let photoEditor = self.storyboard!.instantiateViewControllerWithIdentifier("PhotoEditor") as! PhotoViewController
        photoEditor.photo = image
        let nav = UINavigationController(rootViewController: photoEditor)
        nav.toolbarHidden = false
        photoEditor.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: #selector(PhotoCollectionViewController.modalEditorDidCancel(_:)))
        photoEditor.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Save, target: self, action: #selector(PhotoCollectionViewController.modalEditorDidSave(_:)))
        self.navigationController?.presentViewController(nav, animated: true, completion: nil)

        _photoEditor = photoEditor
    }

    @objc private func modalEditorDidCancel(sender: AnyObject?)
    {
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
        _photoEditor = nil
    }

    @objc private func modalEditorDidSave(sender: AnyObject?)
    {
        assert(_photoEditor != nil)
        let photo = _photoEditor!.photo!
        _images.append(photo)
        self.collectionView?.reloadData()
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
        _photoEditor = nil
    }

}
