// Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

func -=(inout obj1: CGPoint, obj2: CGPoint)
{
    obj1.x -= obj2.x
    obj1.y -= obj2.y
}

var p1 = CGPointMake(10, 10)
var p2 = CGPointMake(20, 20)
p2 -= p1
p2

