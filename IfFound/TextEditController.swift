//
//  TextEditController.swift
//  IfFound
//
//  Created by Darren Stone on 2015-03-03.
//  Copyright (c) 2015 Darren Stone. All rights reserved.
//

import UIKit

class TextEditController: UIViewController
{
    @IBOutlet private var _textView: UITextView!
    @IBOutlet private var _backgroundSwitch: UISwitch!

    override func viewWillAppear(animated: Bool)
    {
        _textView.text = AppModel.sharedInstance.lockScreenText
        _textView.becomeFirstResponder()
        self.preferredContentSize = CGSize(width:320, height:300)
//        var size = self.view.systemLayoutSizeFittingSize(CGSizeMake(320.0, 500.0))
//        println("Fitting Size: \(size)")
    }

    override func viewWillDisappear(animated: Bool)
    {
        AppModel.sharedInstance.lockScreenText = _textView.text
    }
}
