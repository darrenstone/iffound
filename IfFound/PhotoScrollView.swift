//
//  PhotoScrollView.swift
//  IfFound
//
//  Created by Darren Stone on 2015-03-02.
//  Copyright (c) 2015 Darren Stone. All rights reserved.
//

import UIKit

class PhotoScrollView: UIScrollView
{
    //
    // Private Properties
    //
    private var _imageView = UIImageView()
    private var _textView = PhotoTextView(frame: CGRectZero)
    private var _layoutSize = CGSizeZero
    private var _textFrame = CGRectZero

    //
    // Public Properties
    //
    var textMargin : CGFloat = 135.0
    var text : String {
        get {
            return _textView.text
        }
        set(newText) {
            _textView.text = newText;
            self.setNeedsLayout()
        }
    }
    var tapHandler: (() -> Void)?

    //
    // Calculated Properties
    //
    var image : UIImage? {
        get {
            return _imageView.image
        }
        set(newImage) {
            self.zoomScale = 1.0
            self.contentOffset = CGPointZero
            _imageView.image = newImage
            _imageView.sizeToFit()
            _imageView.frame = CGRect(origin: CGPointZero, size: _imageView.frame.size)
            self.contentSize = _imageView.bounds.size
            self.resetZoom(false)
            self.setNeedsLayout()
        }
    }

    //
    // Init
    //
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder:aDecoder)
        self.commonInit()
    }

    private func commonInit()
    {
        self.delegate = self
        self.addSubview(_imageView)
        self.addSubview(_textView)

        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(PhotoScrollView.didDoubleTap(_:)))
        doubleTap.numberOfTapsRequired = 2
        self.addGestureRecognizer(doubleTap)

        let singleTap = UITapGestureRecognizer(target: self, action: #selector(PhotoScrollView.didTapText(_:)))
        _textView.addGestureRecognizer(singleTap)

        doubleTap.requireGestureRecognizerToFail(singleTap)
    }


    //
    // Public Methods
    //
    func saveImage() -> UIImage?
    {
        guard let viewImage = _imageView.image else
        {
            return nil
        }

        let zoom = self.zoomScale
        let scale = UIScreen.mainScreen().scale
        let size = UIScreen.mainScreen().bounds.size
        let offset = self.contentOffset

        UIGraphicsBeginImageContextWithOptions(size, true, scale)
        let ctx = UIGraphicsGetCurrentContext()

        // Image
        CGContextSaveGState(ctx)
        CGContextTranslateCTM(ctx, -offset.x, -offset.y)
        CGContextScaleCTM(ctx, zoom, zoom)
        viewImage.drawAtPoint(CGPointZero)
        CGContextRestoreGState(ctx)

        // Text
        CGContextSaveGState(ctx)
        let textFrame = _textView.frame - offset
        _textView.drawViewHierarchyInRect(textFrame, afterScreenUpdates: false)
        CGContextRestoreGState(ctx)

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }

}

//
// Scrollview Layout
//
extension PhotoScrollView : UIScrollViewDelegate
{
    override func setNeedsLayout()
    {
        _layoutSize = CGSizeZero
        super.setNeedsLayout()
    }

    override func layoutSubviews()
    {
        super.layoutSubviews()

        let bounds = self.bounds
        let offset = self.contentOffset

        if _layoutSize != bounds.size
        {
            _layoutSize = bounds.size

            _textView.sizeToFit(width: bounds.size.width)
            _textFrame = _textView.frame
            _textFrame.ds_centerXInsideBounds(bounds)
            _textFrame.ds_maxYInsideBounds(bounds, offset: self.textMargin)
        }

        let textFrame = _textFrame + offset
        _textView.frame = textFrame

        //print("Image Frame: \(_imageView.frame)")
    }

    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView?
    {
        return _imageView
    }

    @objc private func didDoubleTap(sender: UITapGestureRecognizer?)
    {
        self.resetZoom(true)
    }

    @objc private func didTapText(sender: UITapGestureRecognizer?)
    {
        if let handler = self.tapHandler
        {
            handler()
        }
    }

    private func resetZoom(animated: Bool)
    {
        if let imageSize = _imageView.image?.size where imageSize.height > 0.0
        {
            let screenSize = UIScreen.mainScreen().bounds.size
            let hScale = screenSize.height / imageSize.height
            let vScale = screenSize.width / imageSize.width
            let scale = max(hScale, vScale)
            self.setZoomScale(scale, animated: animated)
            self.minimumZoomScale = scale
        }
    }
}