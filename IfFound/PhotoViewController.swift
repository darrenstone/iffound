//
//  PhotoViewController.swift
//  IfFound
//
//  Created by Darren Stone on 2015-03-01.
//  Copyright (c) 2015 Darren Stone. All rights reserved.
//

import UIKit

private var KVOContext = "KVOPhotoViewController"

class PhotoViewController: UIViewController
{
    var photo : UIImage? {
        didSet {
            self.updatePhoto()
        }
    }

    @IBOutlet private weak var _scrollView : PhotoScrollView!
    @IBOutlet private weak var _cameraButton : UIBarButtonItem!
    @IBOutlet private weak var _editButton : UIBarButtonItem!
    @IBOutlet private weak var _saveButton : UIBarButtonItem!
    @IBOutlet private weak var _hintLabel: UILabel!
    @IBOutlet private weak var _versionLabel: UILabel!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false

        _scrollView.tapHandler = { [weak self] in
            if self != nil
            {
                self!.didTapEditButton(self!._editButton)
            }
        }

        self.navigationItem.leftBarButtonItems = [_cameraButton, _editButton]
        _versionLabel.text = AppModel.sharedInstance.copyright
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

    override func viewWillAppear(animated: Bool)
    {
        AppModel.sharedInstance.addObserver(self, forKeyPath: AppModel.KVO.lockScreenText, options: [], context: &KVOContext)
        self.updatePhoto()
    }

    override func viewWillDisappear(animated: Bool)
    {
        AppModel.sharedInstance.removeObserver(self, forKeyPath: AppModel.KVO.lockScreenText)
    }
}

extension PhotoViewController
{
    @IBAction func save(sender: AnyObject?)
    {
        assert(sender is UIBarButtonItem)

        if let image = _scrollView.saveImage()
        {
            if let data = UIImageJPEGRepresentation(image, 0.7)
            {
                let ui = UIActivityViewController(activityItems: [data], applicationActivities: nil)
                ui.modalPresentationStyle = .Popover
                self.navigationController?.presentViewController(ui, animated: true, completion: nil)

                let presentation = ui.popoverPresentationController
                presentation?.permittedArrowDirections = .Any
                presentation?.barButtonItem = sender as? UIBarButtonItem
            }
        }
    }

    @IBAction private func didTapEditButton(sender: AnyObject)
    {
        if UIDevice.currentDevice().isIPad()
        {
            self.performSegueWithIdentifier("PopoverTextEditorSegue", sender: sender)
        }
        else
        {
            self.performSegueWithIdentifier("ShowTextEditorSegue", sender: sender)
        }
    }

    private var lockScreenTextOrMessage: String {
        var text = AppModel.sharedInstance.lockScreenText
        if text.characters.count <= 0
        {
            text = "Tap here to set text."
        }
        return text
    }

    private func updatePhoto()
    {
        _scrollView.image = self.photo;
        _scrollView.text = self.lockScreenTextOrMessage
        _saveButton.enabled = self.photo != nil
        _hintLabel.hidden = self.photo != nil
    }


    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>)
    {
        guard let keyPath = keyPath, object = object as? NSObject where context == &KVOContext else
        {
            return
        }

        if object == AppModel.sharedInstance && keyPath == AppModel.KVO.lockScreenText
        {
            _scrollView?.text = AppModel.sharedInstance.lockScreenText
        }
    }
}

extension PhotoViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    @IBAction func choosePhoto(sender: AnyObject?)
    {
        let chooser = UIImagePickerController()
        chooser.delegate = self // TODO: Crashes Swift 2 Beta 1 with optimization enabled.

        assert(sender is UIBarButtonItem)
        chooser.modalPresentationStyle = .Popover
        self.presentViewController(chooser, animated: true, completion: nil)

        let presentation = chooser.popoverPresentationController
        presentation?.permittedArrowDirections = .Any
        presentation?.barButtonItem = sender as? UIBarButtonItem
    }

    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?)
    {
        self.dismissViewControllerAnimated(true, completion: nil)
        self.photo = image
    }
}

